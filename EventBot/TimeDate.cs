﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EventBot
{
    public static class TimeDate
    {
        public static bool IsValidDate(string date)
        {
            DateTime possibleDate;
            string[] validDates = { "dd.MM.yyyy", "dd-MM-yyyy", "dd/MM/yyyy" };
            return DateTime.TryParseExact(date, validDates, new CultureInfo("cz"), DateTimeStyles.None, out possibleDate);
        }

        public static bool IsValidTime(string time)
        {
            DateTime possibleTime;
            string[] validTimes = { "HH:mm" };
            return DateTime.TryParseExact(time, validTimes, new CultureInfo("cz"), DateTimeStyles.None, out possibleTime);
        }
    }
}
