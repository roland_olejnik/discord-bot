﻿using Discord.Commands;
using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord.WebSocket;

namespace EventBot
{
    public class Commands : ModuleBase<SocketCommandContext>
    {
        //List of all possible roles
        private static readonly string[] allRoles = { "nature", "movie", "pub", "party", "chill", "sport", "food", "games" };
        //List of banned bots
        private static ulong[] bannedBots = { };
        public static string[] AllRoles { get; }
        public static ulong[] GetBannedBots() { return bannedBots; }

        //sets roles for main user
        [Command("set")]
        [Summary("Gives roles to user")]
        public async Task SetRole([Remainder][Summary("Roles written by user")] string roles = "")
        {
            await Context.Message.DeleteAsync();
            if (Context.Channel.ToString() != "setup")  //checks if command is used in setup
            {
                var errMessage = await Context.Channel.SendMessageAsync("You can assign role only in #setup");
                await Task.Delay(5000);
                await errMessage.DeleteAsync();
                return;
            }
            var user = Context.User;
            string currentRoles = string.Join(' ', (Context.User as SocketGuildUser).Roles);
            string[] currentRolesList = currentRoles.Split(' ');
            foreach (string i in allRoles) //removes all roles
            {
                var role = Context.Guild.Roles.FirstOrDefault(x => x.Name == i);
                await (user as IGuildUser).RemoveRoleAsync(role);
            }
            if (roles == "") // if no roles are used, then it will assign all possible roles
            {
                foreach (string i in allRoles)
                {
                    var role = Context.Guild.Roles.FirstOrDefault(x => x.Name == i);
                    await Context.Channel.SendMessageAsync($"Role added: {i}");
                    await (user as IGuildUser).AddRoleAsync(role);
                }
            }
            else //If roles are used, then it will assign those roles
            {
                string[] commandRoles = roles.Split(' ');
                foreach (string i in commandRoles)
                {
                    try 
                    {
                        var role = Context.Guild.Roles.FirstOrDefault(x => x.Name == i);
                        await (user as IGuildUser).AddRoleAsync(role);
                        await Context.Channel.SendMessageAsync($"Role added: {i}");
                    }
                    catch(NullReferenceException)
                    {
                        var errMessage = await Context.Channel.SendMessageAsync("Bad role(s)");
                        await Task.Delay(5000);
                        await errMessage.DeleteAsync();
                    }
                }
            }
        }

        //Creates new event
        [Command("new")]
        [Summary("Makes new event")]
        public async Task SetEvent([Remainder][Summary("Event info written by user")] string eventInfo)
        {
            if (Context.Channel.ToString() != "events") //Check if even is created in correct channel
            {
                var errMessage = await Context.Channel.SendMessageAsync("You can make events only in #events");
                await Task.Delay(5000);
                await errMessage.DeleteAsync();
                return;
            }
            await Context.Message.DeleteAsync();
            string[] eventInfos = eventInfo.Split(' ');
            try //Checks event info for correct role, date, time and valid integer
            {
                if (!(allRoles.Any(i => i == eventInfos[0]))
                    || !(TimeDate.IsValidDate(eventInfos[3]))
                    || !(TimeDate.IsValidTime(eventInfos[4]))
                    || Convert.ToInt32(eventInfos[5]) <= 0
                    )
                {
                    throw new ArgumentException();
                }

                var eventMsg = await Context.Channel.SendMessageAsync($"{eventInfos[0]}\n{eventInfos[1]}\n{eventInfos[2]}\n{eventInfos[3]}\n{eventInfos[4]}\n{eventInfos[5]}\n");
                await Task.Delay(4000); //Waits for likes, to go into announcements
                var emoji = new Emoji("👍");
                if (eventMsg.Reactions[emoji].ReactionCount >= Convert.ToInt32(eventInfos[5]))
                {
                    var announcmentsChannel = Context.Guild.GetChannel(811291007583911997) as ISocketMessageChannel;
                    await announcmentsChannel.SendMessageAsync($"{eventInfos[0]}\n{eventInfos[1]}\n{eventInfos[2]}\n{eventInfos[3]}\n{eventInfos[4]}\n{eventInfos[5]}\n");
                }

            }
            catch(IndexOutOfRangeException)
            {
                var errMessage = await Context.Channel.SendMessageAsync("Bad number of arguments in command (new)");
                await Task.Delay(5000);
                await errMessage.DeleteAsync();
            }
            catch(ArgumentException)
            {
                var errMessage = await Context.Channel.SendMessageAsync("Arguments provided are not correct");
                await Task.Delay(5000);
                await errMessage.DeleteAsync();
            }
            catch(Exception)
            {
                var errMessage = await Context.Channel.SendMessageAsync("Your arguments are not valid or something went horribly wrong. Please try to provide correct values.");
                await Task.Delay(5000);
                await errMessage.DeleteAsync();
            }

        }

        [Command("ban")]
        [Summary("Bans chosen bot")]
        public async Task BanBot([Remainder][Summary("Chosen bot ")] SocketGuildUser botName)
        {
            await Context.Message.DeleteAsync();
            if (Context.Channel.ToString() != "setup") //Checks for correct channel
            {
                var errMessage = await Context.Channel.SendMessageAsync("You can ban only in #setup");
                await Task.Delay(5000);
                await errMessage.DeleteAsync();
                return;
            }
            if (!(botName.IsBot)) //Checks if user we want to ban is bot
            {
                var errMessage = await Context.Channel.SendMessageAsync("User is not a bot");
                await Task.Delay(5000);
                await errMessage.DeleteAsync();
                return;
            }
            bannedBots.Append(botName.Id);
            await Context.Channel.SendMessageAsync($"Banned bot name: {botName.Username}");
            await Context.Channel.SendMessageAsync($"Banned bot ID: {botName.Id}");
        }

    }



}
