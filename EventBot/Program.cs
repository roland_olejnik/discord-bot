﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using EventBot;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Threading.Tasks;


public class Program
{
    public static void Main(string[] args)
        => new Program().MainAsync().GetAwaiter().GetResult();

    public async Task MainAsync()
    {
        var client = new DiscordSocketClient();
        var commandService = new CommandService();
        var serviceCollection = new ServiceCollection()
            .AddSingleton(client)
            .AddSingleton(commandService)
            .BuildServiceProvider();

        // Log information to the console
        client.Log += Log;

        // Read the token for your bot from file
        var token = File.ReadAllText("token.txt");

        // Log in to Discord
        await client.LoginAsync(TokenType.Bot, token);

        // Start connection logic
        await client.StartAsync();

        // Here you can set up your event handlers
        var commsHandler = new CommandHandler(client, commandService, serviceCollection);

        await commsHandler.SetupAsync();

        // Block this task until the program is closed
        await Task.Delay(-1);
    }

    private static Task Log(LogMessage message)
    {
        Console.WriteLine(message.ToString());
        return Task.CompletedTask;
    }
}