﻿using Discord.Commands;
using Discord.WebSocket;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using Discord;

namespace EventBot
{
    public class CommandHandler
    {
        private readonly DiscordSocketClient client;
        private readonly CommandService commandService;
        private readonly IServiceProvider serviceCollection;
        private static readonly string[] allRoles = { "nature", "movie", "pub", "party", "chill", "sport", "food", "games" };

        //ID of the main user, you should change it to the ID of account you intend to use !!!!!!!!!!!
        private readonly ulong authorId = 218770884909989890;
        public ulong AuthorId { get; }

        // Retrieve client and CommandService instance via constructor
        public CommandHandler(DiscordSocketClient client, CommandService commandService, IServiceProvider serviceCollection)
        {
            this.client = client;
            this.commandService = commandService;
            this.serviceCollection = serviceCollection;
        }

        public async Task SetupAsync()
        {
            // Hook the MessageReceived event into our command handler
            client.MessageReceived += HandleCommandAsync;
            client.MessageReceived += HandleEventAsync;

            // Here we discover all of the command modules in the entry assembly and load them
            await commandService.AddModulesAsync(assembly: Assembly.GetEntryAssembly(), serviceCollection);
        }

        private async Task HandleEventAsync(SocketMessage messageParam) //Checks for events
        {
            var message = messageParam as SocketUserMessage;
            if (message == null) return;

            bool roleMatch = false;
            var context = new SocketCommandContext(client, message);
            int argPos = 0;

            if (message.HasCharPrefix('.', ref argPos) || message.Channel.Name != "events") //Check if it is not command and if the event is used in correct room
            {
                return;
            }

            string[] messageParts = message.Content.Split('\n');
            var author = context.Guild.GetUser(authorId).Roles;

            foreach (SocketRole role in author) //Checks if role used in event is the same as the main user has assigned
            {
                if (role.Name == messageParts[0])
                {
                    roleMatch = true;
                }
            }
            ulong[] bannedBots = Commands.GetBannedBots();
            if (roleMatch && message.Channel.Name == "events" && !(bannedBots.Contains(message.Author.Id))) //Checks if we have correct role, correct room and bot is not banned
            {
                var emoji = new Emoji("👍");
                await message.AddReactionAsync(emoji);
            }
        }

        private async Task HandleCommandAsync(SocketMessage messageParam)
        {
            // Don't process the command if it was a system message
            var message = messageParam as SocketUserMessage;
            if (message == null) return;

            // Create a number to track where the prefix ends and the command begins
            int argPos = 0;

            //Check if message can be event from another bot
            if (message.HasCharPrefix('.', ref argPos) &&
                message.Author.IsBot)
            {
                return;
            }

            // Determine if the message is a command based on the prefix and make sure no bots trigger commands
            if (!message.HasCharPrefix('.', ref argPos) &&
                !message.Author.IsBot)
            {
                return;
            }

            // Create a WebSocket-based command context based on the message
            var context = new SocketCommandContext(client, message);

            //Checks if the command was made by the main user
            if (context.User.Id != authorId )
            {
                return;
            }

            var messageResult = await commandService.ExecuteAsync(context, argPos, serviceCollection);

            if (!messageResult.IsSuccess)
            {
                var errMessage = await context.Channel.SendMessageAsync(messageResult.ErrorReason);
                await Task.Delay(5000);
                await errMessage.DeleteAsync();
                Console.WriteLine(messageResult.ErrorReason);
            }
        }
    }
}
